# Web Clipper's website 

## Description
This page has code of Web Clipper website.
Web Clipper is to take screenshot of webpage with entire page!

## URL
[https://webclipper.micchan.com](https://webclipper.micchan.com "Web Clipper's website")

## E-mail
[hello@micchan.com](mailto:hello@micchan.com "We'd love to hear from you. this is E-mail address for feedback and etc.")

Enjoy Web Surfing :)
